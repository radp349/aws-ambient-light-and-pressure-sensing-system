# Ambient Light and Pressure Sensing System

**Create a platform using M5Core with two sensors: QMP6988 for pressure measurement and BH1750FVI-TR for ambient light sensing.**

- Program the M5Core to gather data from the pressure sensor and ambient light sensor. Format the sensor data into JSON messages. Send the data to AWS Cloud using MQTT protocol. Implement AWS services for receiving and storing the data in S3 buckets.

- Mathematical Transformation: Calculate the average light intensity over a day and correlate it with pressure changes. This simple analysis can be useful for studying environmental patterns (explore how atmospheric pressure variations might affect or correlate with light conditions).


**Przebieg ciśnienia z soboty 03.02.2024 od godziny 14 do północy**

![](images/Aspose.Words.65a12ad0-d345-449b-970e-108d707f3b4d.001.jpeg)

**Przebieg jasności z soboty 03.02.2024 od godziny 14 do północy**

![](images/Aspose.Words.65a12ad0-d345-449b-970e-108d707f3b4d.002.jpeg)

**Korelacja + wartości oczekiwane:**
```
Średnia jasność: 36.451432734337054 

Średnie ciśnienie 100364.69492472075 

Korelacja między ciśnieniem a jasnością: -0.9096551683040444
```
![](images/Aspose.Words.65a12ad0-d345-449b-970e-108d707f3b4d.003.jpeg) ![](images/Aspose.Words.65a12ad0-d345-449b-970e-108d707f3b4d.004.jpeg)


